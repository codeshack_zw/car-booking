# Car Booking

Small Applicaton to reserve a company car for use

#Installation Steps

###storing application files in your web root
The contents of the zip file need to be placed in a folder called "car_booking" in your webroot. A screenshot called webroot_structure.png
is in the installation files folder to show the desired folder structure in the webroot folder. 
Ideally you should be able to reach the login page by using a URL request like this e.g. (assuming you are using WAMP/XAMPP as your web host)

--> http://localhost:8080/car_booking/login.php

##Setting up MYSQL database
You will find two self contained SQL files in the installation files folder. Before running these files ensure you have a mysql user
with suitable privileges. You could use the root user for testing this application but in a real world situation restrictive GRANTS
would need to be enforced. 

You need to create a database called "car_booking" with a character set of utf8 and a colation of utf8_bin.

You may need to run the create_database.sql file to create a database called "car_booking" if you had not created the database already.

You should be now able to run the car_booking.sql file once the car_booking database is created and selected. This file will create
the needed tables and stored functions for the application.

###Creating application users
Once the database is created you need to create an admin user as well as at least one non admin user. You can use any one of the two methods
detailed below

#### Getting your hands dirty in SQL
This will be achieved by running a stored
function we imported earlier that requires the users email address, password to be used for login and the users admin status in that order.
Substitute the email address and password values as you please and you can add as many users as you desire. 

SELECT user_create('<email_address>','<password>','N') as new_id;
SELECT user_create('<email_address>','<password>','N') as new_id;
SELECT user_create('<email_address>','<password>','N') as new_id;
SELECT user_create('<email_address>','<password>','Y') as new_id;
SELECT user_create('<email_address>','<password>','N') as new_id;

An example of a user creation query will look like

SELECT user_create('russellmazonde@gmail.com','mysimplepassword','N') as new_id;  #creates non admin user

OR

SELECT user_create('russellmazonde@gmail.com','mysimplepassword','Y') as new_id;  #creates admin user

#### Using the user creation page
******This method requires you to setup the database ini file following the guidelines below before you can use this page to create users 
Visit the user creation page located on the relative path /car_booking/createUser.php.
On my development machine I found the page here http://localhost:8080/car_booking/createUser.php but you may need to substitute the
domain values to suit your server.
Once the page is open type in the email address to be used, the password to be used and confirm the password by typing it in again.
Then select if the user is admin or not. After filling in these fields you click the "Create User" button and the user will be added.

###Setting up database ini file
The file /car_booking/ini/conn.ini contains the database credentials to be used for connecting to the database. The ini files work on as a key
value set with e.g the key being server_name and the value being localhost. Open the file and replace the values of the server_name, username
and password keys with the values for your mysql user on your server. 

##Good to go
Once these steps are carried out you should be able to visit the login page and use the email address and password you added to the database as a user
earlier to login and start using the application .

If there are any problems please do let me know so I can attend to them for you.

##Take note***
Sending of emails will not work on a host that does have a webserver setup. A XAMPP/WAMP host will fail to send the emails generated unless configured
to send through an existing mail server .e.g. google. https://shellcreeper.com/enable-send-email-in-xampp/ provided insight on how to setup such a host for
enabling sending of emails.

I have also added a screenshot folder so you can quickly review the UI for the application. The folder is inside the installation_files folder and its called
screenshots.

