<?php
require_once('shared/session_lib.php');
require "readIni.php";
require "emailSender.php";

ini_set('display_errors', '0');
//no parameters required, just call the function
$ini = getIni();

//access database credentials using ini to avoid using literals in code for sensitive info
$MYSQLCONN = mysqli_connect($ini["db"]["server_name"],
                            $ini["db"]["username"],
                            $ini["db"]["password"],
                            $ini["db"]["database"]);

// Check connection
if (!$MYSQLCONN){
    echo returnJsonError("Failed to connect to server, please try again", true);
    return;
}

$_DATE = $_POST['ts']; // get post values
if(!isset($_DATE)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid ts parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_DATE = html_entity_decode($_DATE);
$_DATE = mysqli_real_escape_string($MYSQLCONN, $_DATE);

$_USER = $_POST['east']; // get post values
if(!isset($_USER)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid user parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_USER = html_entity_decode($_USER);
$_USER = mysqli_real_escape_string($MYSQLCONN, $_USER);

$_VEHICLE = $_POST['vehicle']; // get post values
if(!isset($_VEHICLE)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid vehicle parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_VEHICLE = html_entity_decode($_VEHICLE);
$_VEHICLE = mysqli_real_escape_string($MYSQLCONN, $_VEHICLE);

$_DURATION = $_POST['duration']; // get post values
if(!isset($_DURATION)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid duration parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_DURATION = html_entity_decode($_DURATION);
$_DURATION = mysqli_real_escape_string($MYSQLCONN, $_DURATION);

$_DISTANCE = $_POST['distance']; // get post values
if(!isset($_DISTANCE)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid distance parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_DISTANCE = html_entity_decode($_DISTANCE);
$_DISTANCE = mysqli_real_escape_string($MYSQLCONN, $_DISTANCE);

$_REASON = $_POST['reason']; // get post values
if(!isset($_REASON)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid reason parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_REASON = html_entity_decode($_REASON);
$_REASON = mysqli_real_escape_string($MYSQLCONN, $_REASON);

$current = strtotime(date("Y-m-d"));
$date    = strtotime($_DATE);

$datediff = $date - $current;
$difference = floor($datediff/(60*60*24));

if($difference > -1){
    $sql =<<<END
        INSERT INTO car_booking.vehicle_booking (booking_user, vehicle_id, booking_date, duration, distance, reason, cancelled)
        VALUES('$_USER', (SELECT vehicle_id FROM car_booking.vehicle WHERE name='$_VEHICLE'), '$_DATE', '$_DURATION', '$_DISTANCE', '$_REASON', 'N' )
        ON DUPLICATE KEY UPDATE distance='$_DISTANCE', reason='$_REASON', cancelled='N', duration='$_DURATION' ;
END;
    
    mysqli_query($MYSQLCONN, $sql) or die(returnJsonError("Failed to add vehicle booking.", true));
    
    $_DATE = date("Y-m-d", strtotime($_DATE));
    
    $message =<<<END
    Dear User,<br><br>
    Your booking for use of the $_VEHICLE company car on $_DATE for a journey has been added/updated. The journey is $_DISTANCE km long and
    has an estimated duration of $_DURATION hours. The stated reason for the journey is: $_REASON.
    <br>
    Have a safe journey. Bye!
    <br><br>
    Car Booking System
END;

    //send email to user and admin
    sendEmail(getEmailAddresses($MYSQLCONN, $_USER), $message);
    
    $response["error"] = false;
    $response["error_msg"] = "Your booking for the $_VEHICLE on $_DATE has been added/updated.";
    echo json_encode($response);
}else{
    $response["error"] = true;
    $response["error_msg"] = "You cannot add or update records for an older time.";
    echo json_encode($response);
}
    
$MYSQLCONN->close();
return;
?>