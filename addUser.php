<?php
require_once('shared/session_lib.php');
require "readIni.php";

ini_set('display_errors', '0');
//no parameters required, just call the function
$ini = getIni();

//access database credentials using ini to avoid using literals in code for sensitive info
$MYSQLCONN = mysqli_connect($ini["db"]["server_name"],
                            $ini["db"]["username"],
                            $ini["db"]["password"],
                            $ini["db"]["database"]);

// Check connection
if (!$MYSQLCONN){
    echo returnJsonError("Failed to connect to server, please try again", true);
    return;
}

$_USER = $_POST['user']; // get post values
if(!isset($_USER)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid user parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_USER = html_entity_decode($_USER);
$_USER = mysqli_real_escape_string($MYSQLCONN, $_USER);

$_PASSWORD = $_POST['password']; // get post values
if(!isset($_PASSWORD)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid password parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_PASSWORD = html_entity_decode($_PASSWORD);
$_PASSWORD = mysqli_real_escape_string($MYSQLCONN, $_PASSWORD);

$_ISADMIN = $_POST['ad']; // get post values
if(!isset($_ISADMIN)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid ad parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_ISADMIN = html_entity_decode($_ISADMIN);
$_ISADMIN = mysqli_real_escape_string($MYSQLCONN, $_ISADMIN);

$sql =<<<END
    SELECT user_create('$_USER','$_PASSWORD','$_ISADMIN') as new_id;  #creates admin user
END;
    
mysqli_query($MYSQLCONN, $sql) or die(returnJsonError("Failed to create new user. User may already exist.", true));

$response["error"] = false;
$response["error_msg"] = "User $_USER has been added";
echo json_encode($response);

$MYSQLCONN->close();
return;
?>
