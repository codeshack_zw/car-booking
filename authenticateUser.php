<?php
    require_once('shared/session_lib.php');
    require "readIni.php";
    
    ini_set('display_errors', '0');
    //no parameters required, just call the function
    $ini = getIni();
    
    //access database credentials using ini to avoid using literals in code for sensitive info
    $MYSQLCONN = mysqli_connect($ini["db"]["server_name"],
                                $ini["db"]["username"],
                                $ini["db"]["password"],
                                $ini["db"]["database"]);
    
    // Check connection
    if (!$MYSQLCONN){
        echo returnJsonError("Failed to connect to server, please try again", true);
        return;
    }

    // Get login email and password from a submitted form
    // get Form (POST/GET) variables.
    $login_email = isset($_REQUEST['emailAddress']) ? $_REQUEST['emailAddress'] : null;
    $login_pwd = isset($_REQUEST['password']) ? $_REQUEST['password'] : null;
    $login_days = isset($_REQUEST['login_days']) ? $_REQUEST['login_days'] : null;
    // and escape these inputs ready for MySql
    $login_email = mysqli_real_escape_string($MYSQLCONN, $login_email);
    $login_pwd = mysqli_real_escape_string($MYSQLCONN, $login_pwd);
    $login_days = mysqli_real_escape_string($MYSQLCONN, $login_days);


    $login_url = "login.php";
    $index_url = "index.php";

    ///////////////////////////////////
    // validate submitted email and password //
    ///////////////////////////////////
    $lc_sql =<<<END
        SELECT car_booking.user_validate_password('$login_email','$login_pwd') AS valid_user_id;
END;

    $lc_result = mysqli_query($MYSQLCONN, $lc_sql) or die(returnJsonError("Failed to validate login email and password ", true));
    while($lc_row = mysqli_fetch_array($lc_result)){
        $valid_user_id = $lc_row['valid_user_id'];
    }
    // free up results
    mysqli_free_result($lc_result); // free up results

    /////////////////////////////
    // Now create hashed_session and pass cookie
    /////////////////////////////
    if($valid_user_id != null) {
            // create random string for cookie
        $cookie_rand_string  = randomAlphaNum(32);
        
        // Now hash the random string and get the session_id
        $lc_sql =<<<END
    SELECT car_booking.user_create_session($valid_user_id,'$cookie_rand_string') AS new_session_id;
END;
        $lc_result = mysqli_query($MYSQLCONN, $lc_sql) or die(returnJsonError("Failed to create session", true));
        while($lc_row = mysqli_fetch_array($lc_result)){
            $new_session_id = $lc_row['new_session_id'];
        }
        // free up query results
        mysqli_free_result($lc_result);
        // we have a new session_id, so set cookie
        if($new_session_id) {
            // convert to int. Sets to 0 if string is empty.
            $login_days = (int)$login_days;
            // expire in n days
            if($login_days != null && $login_days > 0) {
                // send cookie
                //setcookie( "SESSION_ID", $cookie_rand_string,strtotime( '+'.$login_days.' days' ),'/', null,1,1);
                 setcookie( "SESSION_ID", $cookie_rand_string,strtotime( '+'.$login_days.' days' ));
                
                // update expiry in session table
                $lc_sql =<<<END
                UPDATE car_booking.user_session SET expires=now() + INTERVAL $login_days DAY
                WHERE session_id=$new_session_id AND user_id=$valid_user_id
END;
                mysqli_query($MYSQLCONN, $lc_sql) or die(returnJsonError("Failed update user_session", true));
            }
            // expire at the end of the current session
            else {
                // send cookie
                //setcookie( "SESSION_ID", $cookie_rand_string,0,'/', null,1,1);
                setcookie( "SESSION_ID", $cookie_rand_string,0);
                
                // update expiry in session table. We set to 1 day max as we don't know how long the current session will last
                $lc_sql =<<<END
                UPDATE car_booking.user_session SET expires=now() + INTERVAL 1 DAY
                WHERE session_id=$new_session_id AND user_id=$valid_user_id
END;

                mysqli_query($MYSQLCONN, $lc_sql) or die(returnJsonError("Failed update user_session", true));
            }
        }
        // no session created, so treat as failed
        // Do something or redirect to login page
        else {
            redirect($login_url);
        }
    }
    // Login has failed, they shouldn't be here
    // Do something or redirect to login page
    else {
        redirect($login_url);
    }

    echo returnJsonError($index_url, false);
?>