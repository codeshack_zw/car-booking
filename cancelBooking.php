<?php
require_once('shared/session_lib.php');
require "readIni.php";
require "emailSender.php";
    
ini_set('display_errors', '0');
//no parameters required, just call the function
$ini = getIni();

//access database credentials using ini to avoid using literals in code for sensitive info
$MYSQLCONN = mysqli_connect($ini["db"]["server_name"],
                            $ini["db"]["username"],
                            $ini["db"]["password"],
                            $ini["db"]["database"]);

// Check connection
if (!$MYSQLCONN){
    echo returnJsonError("Failed to connect to server, please try again", true);
    return;
}

$_DATE = $_POST['ts']; // get post values
if(!isset($_DATE)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid ts parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_DATE = html_entity_decode($_DATE);
$_DATE = mysqli_real_escape_string($MYSQLCONN, $_DATE);

$_USER = $_POST['east']; // get post values
if(!isset($_USER)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid user parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_USER = html_entity_decode($_USER);
$_USER = mysqli_real_escape_string($MYSQLCONN, $_USER);

$_VEHICLE = $_POST['vehicle']; // get post values
if(!isset($_VEHICLE)){
    $response["error"] = true;
    $response["error_msg"] = "Invalid vehicle parameter provided.";
    echo json_encode($response);
    $MYSQLCONN->close();
    return;
}

$_VEHICLE = html_entity_decode($_VEHICLE);
$_VEHICLE = mysqli_real_escape_string($MYSQLCONN, $_VEHICLE);

//check date to make sure its not in the past
$current = strtotime(date("Y-m-d"));
$date    = strtotime($_DATE);

$datediff = $date - $current;
$difference = floor($datediff/(60*60*24));

if($difference > -1){
    $sql =<<<END
        UPDATE car_booking.vehicle_booking SET cancelled='Y' WHERE
        booking_date='$_DATE' AND booking_user='$_USER' AND vehicle_id=(SELECT vehicle_id FROM car_booking.vehicle WHERE name='$_VEHICLE');
END;
    
    mysqli_query($MYSQLCONN, $sql) or die(returnJsonError("Failed to cancel the booking for $_VEHICLE on $_DATE.", true));
    
    $message =<<<END
    Dear User,<br><br>
    Your booking for use of the $_VEHICLE company car on $_DATE for a journey has been cancelled. 
    <br>
    Come again soon. Bye!
    <br><br>
    Car Booking System
END;

    //send email to user and admin
    sendEmail(getEmailAddresses($MYSQLCONN, $_USER), $message);
    
    $response["error"] = false;
    $response["error_msg"] = "Booking for $_VEHICLE on $_DATE has been added.";
    echo json_encode($response);
}else{
    $response["error"] = true;
    $response["error_msg"] = "You cannot cancel bookings for past dates.";
    echo json_encode($response);
}
    
$MYSQLCONN->close();
return;
?>