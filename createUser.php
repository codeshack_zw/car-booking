<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Car Booking: Create User</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="js/createUser.js" async defer></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a id="navigateLogin"href="/car_booking/login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>
  
<div id="dialog"></div>
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-4 sidenav">
    </div>
    <div class="col-sm-4 text-left"> 
        <h1>Create A User</h1>
        <hr>
        <label>Email Address</label>
        <input type="text" id="emailAddress"/>
        <label>Password</label>
        <input type="password" id="password"/>
        <label>Confirm Password</label>
        <input type="password" id="confirmPassword"/>
        <label>Admin User</label>
        <select id="is_admin">
            <option value="Y">Yes</option>
            <option value="N">No</option> 
        </select>
        <input type="submit" value="Create User" id="createUser">
    </div>
    <div class="col-sm-4 sidenav">
    </div>
  </div>
</div>

</body>
</html>
