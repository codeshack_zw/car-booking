<?php
#
# email sender code
# requires two arguments
# 1. comma seperated string of email addresses to send email to as 
# 2. message to send as email
function sendEmail($to, $message){

    $subject = "Company vehicle booking notification";    
    $headers = "From: noreply@carbooking.no" . "\r\n";
    mail($to,$subject,$message,$headers);
}


#fetch email addresses for recepient and admin.
#requires two arguments
# 1. mysql connection
# 2. user to fetch email address for
function getEmailAddresses( $MYSQLCONN, $_USER){
    $sql =<<<END
        SELECT email FROM car_booking.user_info WHERE user_id='$_USER' OR is_admin='Y'
END;
    
    $result = mysqli_query($MYSQLCONN, $sql) or die(returnJsonError("Failed to send user email confirming booking.", true));
    
    $emails = array();
    while($row = mysqli_fetch_array($result)){
        array_push($emails, $row['email']);
    }
    
    mysqli_free_result($result);
    return implode(",",$emails);
}
?>