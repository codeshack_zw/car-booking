<?php
require_once('shared/session_lib.php');
require "readIni.php";

ini_set('display_errors', '0');
//no parameters required, just call the function
$ini = getIni();

//access database credentials using ini to avoid using literals in code for sensitive info
$MYSQLCONN = mysqli_connect($ini["db"]["server_name"],
                            $ini["db"]["username"],
                            $ini["db"]["password"],
                            $ini["db"]["database"]);

// Check connection
if (!$MYSQLCONN){
    echo returnJsonError("Failed to connect to server, please try again", true);
    return;
}

$sql = '';
if(isset($_POST['ts'])){
    $_DATE = $_POST['ts']; // get post values
    $_DATE = html_entity_decode($_DATE);
    $_DATE = mysqli_real_escape_string($MYSQLCONN, $_DATE);
    $_DATE = date("Y-m-d", strtotime($_DATE));

    $sql =<<<END
    SELECT a.booking_user, c.email, b.name, DATE_FORMAT(a.booking_date, "%Y-%m-%d") `booking_date`, a.duration,
    a.distance, a.reason FROM car_booking.vehicle_booking a INNER JOIN
    car_booking.vehicle b ON a.vehicle_id=b.vehicle_id INNER JOIN
    car_booking.user_info c ON c.user_id=a.booking_user WHERE cancelled='N' AND a.booking_date='$_DATE';
END;
}else{
    $sql =<<<END
    SELECT a.booking_user, c.email, b.name, DATE_FORMAT(a.booking_date, "%Y-%m-%d") `booking_date`,
    a.duration, a.distance, a.reason FROM car_booking.vehicle_booking a INNER JOIN car_booking.vehicle b ON
    a.vehicle_id=b.vehicle_id INNER JOIN car_booking.user_info c ON c.user_id=a.booking_user WHERE cancelled='N'
    AND (a.booking_date > NOW() - INTERVAL 1 MONTH);
END;
}

$result = mysqli_query($MYSQLCONN, $sql) or die(returnJsonError("Failed to fetch existing vehicle bookings.", true));

$response["error"] = true;
$response["error_msg"] = "No vehicle Information Found";
$bookingResult = [];
$booking["html"] = "";
$booking["bookForm"] = "";
$count = 0;
while($row = mysqli_fetch_array($result)){
    $response["error"] = false;
    $response["error_msg"] = null;
           
    $booking["id"] = $row['booking_user'];
    $booking["booking_date"] = $row['booking_date'];
    $booking["html"] =<<<END
        <p><a href="#" onclick="viewBooking('{$row['booking_user']}','{$row['email']}','{$row['booking_date']}',
        '{$row['name']}','{$row['duration']}','{$row['distance']}','{$row['reason']}')">
        A booking on {$row['booking_date']} was submitted by <u>{$row['email']}</u>. The
        {$row['name']} vehicle has been booked for a {$row['distance']} KM journey that should take {$row['duration']} hours to complete.</a></p>
        <hr/>
END;
    $bookingResult[$count] = $booking;
    $count++;
}

$response["bookings"] = $bookingResult;
$response["bookForm"] = bookNowForm($MYSQLCONN);
// free up results
mysqli_free_result($result);

echo json_encode($response);
$MYSQLCONN->close();
return;

function bookNowForm($MYSQLCONN){
    $sql =<<<END
    SELECT * FROM 
    car_booking.vehicle;
END;

    $result = mysqli_query($MYSQLCONN, $sql) or die(returnJsonError("Failed to fetch vehicle list.", true));
    $options = "";
    while($row = mysqli_fetch_array($result)){
        $options .=<<<END
            <option value="{$row['vehicle_id']}">{$row['name']}</option>
END;
    }
    
    $html =<<<END
    <label>Booking Date</label>
    <input type="text" id="date" readonly="readonly"/>
    <label>Vehicle</label>
    <select id="vehicle">
        $options
    </select>
    <label>Duration (Hours)</label>
    <input type="text" id="duration"/>
    <label>Distance (Kilometers)</label>
    <input type="text" id="distance"/>
    <label>Reason</label>
    <textarea id="reason"></textarea>
    <input type="button" value="Add/Update Booking" id="submitBooking"/>
    <input type="button" value="Cancel Booking" id="cancelBooking"/>
    <input type="hidden" id="ppk" value="">
END;
    
    mysqli_free_result($result);
    return $html;
}
?>