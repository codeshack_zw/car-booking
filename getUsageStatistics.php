<?php
#
# generate data for highcharts summary chart on home page
#

require_once('shared/session_lib.php');
require "readIni.php";

ini_set('display_errors', '0'); 
//no parameters required, just call the function
$ini = getIni();

//access database credentials using ini to avoid using literals in code for sensitive info
$MYSQLCONN = mysqli_connect($ini["db"]["server_name"],
                            $ini["db"]["username"],
                            $ini["db"]["password"],
                            $ini["db"]["database"]);

// Check connection
if (!$MYSQLCONN){
    echo returnJsonError("Failed to connect to server, please try again", true);
    return;
}

$sql =<<<END
    SELECT count(a.booking_id) 'stats', b.email FROM car_booking.vehicle_booking a
    INNER JOIN car_booking.user_info b ON a.booking_user=b.user_id
    WHERE a.cancelled='N' GROUP BY booking_user
END;

$result = mysqli_query($MYSQLCONN, $sql) or die(returnJsonError("Failed to send user email confirming booking.", true));
    
$rows = [];
while($row = mysqli_fetch_array($result)){
    $rows[] = array($row['email'], floatval($row['stats']), true);
}

echo json_encode($rows);
?>