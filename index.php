<?php
require_once('shared/session_lib.php');
require "readIni.php";

ini_set('display_errors', '0');
//no parameters required, just call the function
$ini = getIni();

//access database credentials using ini to avoid using literals in code for sensitive info
$MYSQLCONN = mysqli_connect($ini["db"]["server_name"],
                            $ini["db"]["username"],
                            $ini["db"]["password"],
                            $ini["db"]["database"]);

if (!$MYSQLCONN){
  echo returnJsonError("Failed to connect to server, please try again", true);
  return;
}

$session = validateSessionPage($MYSQLCONN);
$user_id = $session[0];
$sessionID = $session[1];

$user_id = mysqli_real_escape_string($MYSQLCONN, $user_id);
//get email for user
$sql =<<<END
  SELECT email from car_booking.user_info WHERE user_id='$user_id';
END;

$userEmail='';
$lc_result = mysqli_query($MYSQLCONN, $sql);
while($lc_row = mysqli_fetch_array($lc_result)){
    $userEmail = $lc_row['email'];
}

// free up results
mysqli_free_result($lc_result); // free up results
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Book The Company Car</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/jquery-ui-timepicker-addon.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="js/manageCarBookings.js"></script>
  <script src="js/jquery-ui-timepicker-addon.js"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/highcharts-more.js"></script>
  <link rel="stylesheet" href="css/style.css"/>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="/car_booking/index.php">Home</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a id="logout" href="#"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
<div id="dialog"></div>
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-1 sidenav">
    </div>
    <div class="col-sm-7 text-left"> 
      <h1>Hello <?php echo $userEmail;?></h1>
      <p>You can book the company car for use on a specific day on this page. Do remember to provide the details of your trip. You can also
      cancel a booking you have made as long as it is not before the current date. You can also see other bookings made by other staff but cannot edit or cancel them.
      You along with the vehicle administrator will be sent an email when you book or cancel a booking on a vehicle.</p>
      <hr>
      <h3>Select a date and time below to book the vehicle</h3>
      <div class="parent">
        <div class="narrow">
          <input type="text" id="timeValue" readonly="readonly"/>
          <div id=bookingDate></div>
          <input type="submit" value="Book Vehicle" id="addBooking">
          <input type="hidden" id="east" value="<?php echo $user_id;?>">
        </div>
        <div class="wide" id="booking_result">
        </div>
      </div>
    </div>
    <div class="col-sm-4 sidenav">
      <div id="container3"></div>
    </div>
  </div>
</div>
</body>
</html>
