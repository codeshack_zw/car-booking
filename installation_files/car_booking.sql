SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `create_salt` () RETURNS CHAR(14) CHARSET latin1 BEGIN
	SELECT SUBSTRING(MD5(RAND()) FROM 1 FOR 14) into @code;

    RETURN @code;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `user_create` (`given_email` VARCHAR(254), `given_password` VARCHAR(128), `given_adminAccess` VARCHAR(1)) RETURNS INT(11) BEGIN
      DECLARE new_salt VARCHAR(128);
      DECLARE new_password VARCHAR(128);
      DECLARE new_user_id INTEGER;

      SET new_salt     = create_salt();

			# MySql 5.6x so use SHA2
			SET new_password = SHA2(CONCAT(given_password, new_salt),512);

      INSERT INTO `car_booking`.`user_info` (password_hash, salt, email,is_admin)
      VALUES (new_password, new_salt, given_email,given_adminAccess);

	SET new_user_id = LAST_INSERT_ID();

      RETURN new_user_id;

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `user_create_session` (`given_user_id` INT(11), `cookie_random_string` VARCHAR(128)) RETURNS INT(11) BEGIN
      DECLARE old_user_id INTEGER;
      DECLARE new_session_id INTEGER;
			DECLARE new_salt VARCHAR(128);
			DECLARE encrypted_session_string VARCHAR(128);

			SELECT user_id INTO old_user_id FROM `car_booking`.`user_info` WHERE user_id = given_user_id;

			# Only create session if the old_user_id exists
      IF old_user_id IS NOT NULL THEN

				SET new_salt = create_salt();

				# MySql 5.6x so use SHA2
				SET encrypted_session_string = SHA2(CONCAT(cookie_random_string, new_salt),512);

				# We default to expire in 1 hour
				INSERT INTO `car_booking`.`user_session` (user_id,salt,encrypted_session,expires)
				VALUES (given_user_id,new_salt,encrypted_session_string,now() + INTERVAL 1 HOUR);

				SET new_session_id = LAST_INSERT_ID();

      END IF;


      RETURN new_session_id;

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `user_update_password` (`given_user_id` INT(11), `given_email` VARCHAR(254), `given_password` VARCHAR(128)) RETURNS TINYINT(1) BEGIN
      DECLARE tmp VARCHAR(128);
      DECLARE res BOOL;
      
      SELECT salt INTO tmp FROM `car_booking`.`user_info` WHERE email = given_email;
      
      IF tmp IS NOT NULL THEN
        UPDATE `car_booking`.`user_info` SET password_hash=SHA2(CONCAT(given_password,tmp), 512) 
	WHERE user_id = given_user_id AND email = given_email;
        SET res = TRUE;
      ELSE
        SET res = FALSE;
      END IF;

      RETURN res;

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `user_validate_password` (`given_email` VARCHAR(254), `given_password` VARCHAR(128)) RETURNS INT(11) BEGIN

      DECLARE tmp INT;
      SELECT user_id INTO tmp FROM `car_booking`.`user_info`
      WHERE email = given_email AND password_hash = SHA2(CONCAT(given_password,salt), 512);                  
      RETURN tmp;

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `user_validate_session` (`cookie_random_string` VARCHAR(128)) RETURNS INT(11) BEGIN

      DECLARE this_session_id INT;
      SELECT session_id INTO this_session_id FROM `car_booking`.`user_session`
      WHERE encrypted_session = SHA2(CONCAT(cookie_random_string,salt), 512)
	AND now() <= expires
	LIMIT 1;                  
      RETURN this_session_id;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `email` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `salt` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `password_hash` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `is_admin` enum('Y','N') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_session`
--

CREATE TABLE `user_session` (
  `session_id` int(11) UNSIGNED NOT NULL,
  `salt` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `encrypted_session` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `expires` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `vehicle_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `description` varchar(150) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`vehicle_id`, `name`, `description`) VALUES
(1, 'VW City Golf', '1990 old school ride'),
(2, 'VW Golf GTI', '2016 Edition');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_booking`
--

CREATE TABLE `vehicle_booking` (
  `booking_id` int(10) UNSIGNED NOT NULL,
  `booking_user` int(11) UNSIGNED NOT NULL,
  `vehicle_id` int(11) UNSIGNED NOT NULL,
  `booking_date` date NOT NULL,
  `duration` int(11) NOT NULL,
  `distance` int(11) NOT NULL DEFAULT '0',
  `cancelled` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N',
  `reason` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for table `user_info`
--  
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`,`email`),
  ADD UNIQUE KEY `unique_indexes` (`email`) USING BTREE;

--
-- Indexes for table `user_session`
--
ALTER TABLE `user_session`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `session_string` (`encrypted_session`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`vehicle_id`);

--
-- Indexes for table `vehicle_booking`
--
ALTER TABLE `vehicle_booking`
  ADD PRIMARY KEY (`booking_id`),
  ADD UNIQUE KEY `vehicle_id` (`vehicle_id`,`booking_date`,`booking_user`),
  ADD KEY `bookingUser_FK` (`booking_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_session`
--
ALTER TABLE `user_session`
  MODIFY `session_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `vehicle_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehicle_booking`
--
ALTER TABLE `vehicle_booking`
  MODIFY `booking_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
  
--
-- Constraints for table `vehicle_booking`
--
ALTER TABLE `vehicle_booking`
  ADD CONSTRAINT `bookingUser_FK` FOREIGN KEY (`booking_user`) REFERENCES `user_info` (`user_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `vehicleID_FK` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicle` (`vehicle_id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

