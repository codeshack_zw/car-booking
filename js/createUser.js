$( "#createUser" ).click(function() {
    if ($("#emailAddress").val() !== "" ||
         $("#password").val() !== "" ||
         $("#confirmPassword").val() !== "" ||
         $( "#vehicle option:selected" ).val()) {
        
        if ($("#password").val() !== $("#confirmPassword").val()) {
            $("#dialog").html("<p>The passwords you provided do not match. Please type in a password and confirm it.</p>");
            $("#dialog").dialog({
                modal: true,
                height: 250,
                width: 400,
                resizable: false,
                closeOnEscape: true,
                title: "Error",
                buttons: {
                    Ok: function() {
                        $(this).dialog("close");
                        return;
                    }
                }
            });
            return;
        }
        
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {         
                var resp = JSON.parse(this.response);
                $("#dialog").html(resp["error_msg"]);
                $("#dialog").dialog({
                    modal: true,
                    height: 250,
                    width: 400,
                    resizable: false,
                    closeOnEscape: true,
                    title: "Notification",
                    buttons: {
                        Ok: function() {
                            $(this).dialog("close");
                            clearFields();
                            return;
                        }
                    }
                });
                clearFields();
            }
        };
        
        xhr.onerror = function(){
            $("#dialog").html("<p>An error occured while creating a new user.</p>");
            $("#dialog").dialog({
                modal: true,
                height: 250,
                width: 400,
                resizable: false,
                closeOnEscape: true,
                title: "Error",
                buttons: {
                    Ok: function() {
                        $(this).dialog("close");
                        return;
                    }
                }
            });
            return;
        }
        
        var parm = "user="+ myTrim(escape($("#emailAddress").val())) +
                    "&password=" + myTrim(escape($("#password").val())) +
                    "&ad=" + myTrim(escape($( "#is_admin option:selected" ).val()));
        
        xhr.open("POST", "/car_booking/addUser.php", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(parm);
    }else{
        $("#dialog").html("<p>Please fill in all values on the form before submitting.</p>");
        $("#dialog").dialog({
            modal: true,
            height: 250,
            width: 400,
            resizable: false,
            closeOnEscape: true,
            title: "Error",
            buttons: {
                Ok: function() {
                    $(this).dialog("close");
                    return;
                }
            }
        });
        
        $("#emailAddress").focus();
    }
    
});

function clearFields(){
    $("#emailAddress").val("");
    $("#password").val("");
    $("#confirmPassword").val("");
    $("#emailAddress").focus();
}

/*
 * trim text
 */
function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}