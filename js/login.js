$( "#submitLogin" ).click(function() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {         
            var resp = JSON.parse(this.response);
            if (resp["error"] == false){
                window.location.replace(resp["error_msg"]);    
            }else{
                $("#dialog").html(resp["error_msg"]);
                $("#dialog").dialog({
                    modal: true,
                    height: 250,
                    width: 400,
                    resizable: false,
                    closeOnEscape: true,
                    title: "Login Failed",
                    buttons: {
                        Ok: function() {
                            $(this).dialog("close");
                            return;
                        }
                    }
                });
            }
        }
    };
    
    xhr.onerror = function(){
        $("#dialog").html("<p>An error occured while performing your login request.</p>");
        $("#dialog").dialog({
            modal: true,
            height: 250,
            width: 400,
            resizable: false,
            closeOnEscape: true,
            title: "Login Failed",
            buttons: {
                Ok: function() {
                    $(this).dialog("close");
                    return;
                }
            }
        });
        return;
    }
    
    var persistantLogin = document.getElementById("persistentLogin").checked ? "30" : "0" ;
    var parm = "emailAddress="+ document.getElementById("emailAddress").value +
                "&password=" + document.getElementById("password").value +
                "&login_days=" + persistantLogin;

    xhr.open("POST", "/car_booking/authenticateUser.php", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(parm);
});