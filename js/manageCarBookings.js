var eventDates = {};
var bookNowForm = "";

$( document ).ready(function() {
    getBookedDates();
    initLogoutListener();
    initBookNowListener();
    
    $("#addBooking").hide();
    getUsageData();
});

/*
 * view information for a particular booking. It will open a dialog with populated information
 */
function viewBooking(id, email, date, name, duration, distance, reason){
    $("#dialog").html(bookNowForm)
    $("#dialog").dialog({
        modal: true,
        height: 400,
        width: 400,
        resizable: false,
        closeOnEscape: true,
        title: "View Booking"
    });
    
    $("#date").val(date);
    $("#date").prop("readonly", true);
    
    $('#vehicle option:contains("' + name +'")').prop('selected', true);
    $("#vehicle").attr('disabled', true);
    
    $("#duration").val(duration);
    $("#duration").prop("readonly", true);
    
    $("#distance").val(distance);
    
    $("#reason").val(reason);
    $("#reason").prop("readonly", true);
    
    $("#cancelBooking").css("background-color", "#B22222");
    $("#ppk").val(id);
    
    if ($("#east").val() !==  $("#ppk").val()) {
        $("#cancelBooking").hide();
        $("#submitBooking").hide();
    }else{
        $("#cancelBooking").show();
        $("#submitBooking").show();
        initSubmitBookingListener();
    }
    
    $("#cancelBooking").click(function() {
        if($("#date").val() !== "" &&
            $("#vehicle option:selected" ).text() !== "" &&
            $("#duration").val() !== "" &&
            $("#distance").val() !== "" &&
            $("#reason").val() !== ""){
            
            if ($("#east").val() !==  $("#ppk").val()) {
                $("#dialog").html("<p>Your request could not be completed. You cannot cancel a booking placed by another user</p>");
                $("#dialog").dialog({
                    modal: true,
                    height: 250,
                    width: 400,
                    resizable: false,
                    closeOnEscape: true,
                    title: "Error",
                    buttons: {
                        Ok: function() {
                            $(this).dialog("close");
                            return;
                        }
                    }
                });
                return;
            }
            
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    getUsageData();
                    getBookedDates();
                    var resp = JSON.parse(this.response);
                    $("#dialog").html(resp["error_msg"]);
                    $("#dialog").dialog({
                        modal: true,
                        height: 250,
                        width: 400,
                        resizable: false,
                        closeOnEscape: true,
                        title: "Notification",
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                                return;
                            }
                        }
                    });
                    return;
                }
            };
            
            xhr.onerror = function(){
                $("#dialog").html("<p>An error occured while adding/update your booking</p>");
                $("#dialog").dialog({
                    modal: true,
                    height: 250,
                    width: 400,
                    resizable: false,
                    closeOnEscape: true,
                    title: "An error occured",
                    buttons: {
                        Ok: function() {
                            $(this).dialog("close");
                            return;
                        }
                    }
                });
                return;
            }
            
            var parm = "ts=" + myTrim(escape($("#date").val())) +
                        "&vehicle=" + myTrim(escape($( "#vehicle option:selected" ).text())) +
                        "&east=" + $("#east").val();
                        
            xhr.open("POST", "/car_booking/cancelBooking.php", true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send(parm);
        }else{
            $("#dialog").html("<p>Please fill in all the fields in this form to book a vehicle</p>");
            $("#dialog").dialog({
                modal: true,
                height: 250,
                width: 400,
                resizable: false,
                closeOnEscape: true,
                title: "Error",
                buttons: {
                    Ok: function() {
                        $(this).dialog("close");
                        return;
                    }
                }
            });
        }
        
    });
}

/*
 * fetch data for populating the highcharts pie chart
 */
function getUsageData() {
    var xhr = new XMLHttpRequest();
    
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {         
            var resp = JSON.parse(this.response);
            Highcharts.chart('container3', {
                title: {
                    text: 'Booking Statistics By User'
                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
                series: [{
                    type: 'pie',
                    allowPointSelect: true,
                    data: resp,
                    name: "Bookings",
                    showInLegend: true
                }]
            });
            return;
        }
    };
    
    xhr.onerror = function(){
        $("#dialog").html("<p>An error occured while adding/update your booking</p>");
        $("#dialog").dialog({
            modal: true,
            height: 250,
            width: 400,
            resizable: false,
            closeOnEscape: true,
            title: "Error",
            buttons: {
                Ok: function() {
                    $(this).dialog("close");
                    return;
                }
            }
        });
        return;
    }
                
    xhr.open("POST", "/car_booking/getUsageStatistics.php", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send();
}

/*
 * get dates already booked and mark them on the calendar
 */
function getBookedDates() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {         
            var resp = JSON.parse(this.response);
            bookNowForm = resp["bookForm"];
            
            if (resp["error"] == false){
                //update UI with bookings for that day
                $("#booking_result").html("");
                for(var i = 0; i < resp["bookings"].length; i++){
                    eventDates[ new Date(resp["bookings"][i]["booking_date"] + " 00:00:00")] = new Date(resp["bookings"][i]["booking_date"] + " 00:00:00");
                }
                
                initDatePicker();
            }else{
                $("#booking_result").html("<p>There is no booking for the selected day. Click the Book Vehicle button to book the vehicle for " +
                                            "use.<p>");
                $("#addBooking").show();
                
                initDatePicker();
                return;
            }
        }
    };
    
    xhr.onerror = function(){
        $("#dialog").html("<p>An error occured while fetching booked dates</p>");
        $("#dialog").dialog({
            modal: true,
            height: 250,
            width: 400,
            resizable: false,
            closeOnEscape: true,
            title: "Error",
            buttons: {
                Ok: function() {
                    $(this).dialog("close");
                    return;
                }
            }
        });
        return;
    }
    
    xhr.open("POST", "/car_booking/fetchBookings.php", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send();
}

function initDatePicker(){
    $('#bookingDate').datepicker({
        dateFormat: "yy-mm-dd",
        stepHour: 1,
        stepMinute: 10,
        showMinute: true,
        showSecond: false,
        changeYear: true,
        changeMonth: true,
        showButtonPanel: false,
        onSelect: function(date) {
            $("#timeValue").val(date);
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {         
                    var resp = JSON.parse(this.response);
                    if (resp["error"] == false){
                        //update UI with bookings for that day
                        $("#booking_result").html("");
                        $("#booking_result").html(resp["html"] );
                        for(var i = 0; i < resp["bookings"].length; i++){
                            eventDates[ new Date(resp["bookings"][i]["booking_date"])] = new Date(resp["bookings"][i]["booking_date"]);
                            $("#booking_result").append(resp["bookings"][i]["html"]);
                        }
                        $("#addBooking").hide();
                    }else{
                        $("#booking_result").html("<p>There is no booking for the selected day. Click the Book Vehicle button to book the vehicle for " +
                                            "use.<p>");
                        $("#addBooking").show();
                        return;
                    }
                }
            };
            
            xhr.onerror = function(){
                $("#dialog").html("<p>An error occured while logging out.</p>");
                $("#dialog").dialog({
                    modal: true,
                    height: 250,
                    width: 400,
                    resizable: false,
                    closeOnEscape: true,
                    title: "Error",
                    buttons: {
                        Ok: function() {
                            $(this).dialog("close");
                            return;
                        }
                    }
                });
            }
            
            var parm = "ts="+ escape(date);
            xhr.open("POST", "/car_booking/fetchBookings.php", true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send(parm);
        },
        numberOfMonths: 1,
        beforeShowDay: function( date ) {
            var highlight = eventDates[date];
            if( highlight ) {
                 return [true, "event", 'Tooltip text'];
            } else {
                 return [true, '', ''];
            }
        }
    });
}


/*
* add listener for when the book now button is clicked
*/
function initBookNowListener() {
    $("#addBooking").click(function() {
        $("#dialog").html(bookNowForm)
        $("#dialog").dialog({
            modal: true,
            height: 400,
            width: 400,
            resizable: false,
            closeOnEscape: true,
            title: "Add New Booking"
        });
        
        $("#date").val($("#timeValue").val());
        $("#cancelBooking").hide();
        initSubmitBookingListener();     
    });
}

/*
 * add listener for submitting a booking 
 */
function initSubmitBookingListener() {
    $("#submitBooking").click(function() {
        if($("#date").val() !== "" &&
            $( "#vehicle option:selected" ).text() !== "" &&
            $("#duration").val() !== "" &&
            $("#distance").val() !== "" &&
            $("#reason").val() !== ""){
            
            if (isNaN($("#duration").val()) || isNaN($("#distance").val())) {
                $("#dialog").html("<p>Please ensure that the distance and duration values are numeric.</p>");
                $("#dialog").dialog({
                    modal: true,
                    height: 250,
                    width: 400,
                    resizable: false,
                    closeOnEscape: true,
                    title: "An error occured",
                    buttons: {
                        Ok: function() {
                            $(this).dialog("close");
                            return;
                        }
                    }
                });
                return;
            }
            
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    getUsageData();
                    getBookedDates();
                    var resp = JSON.parse(this.response);
                    $("#dialog").html(resp["error_msg"]);
                    $("#dialog").dialog({
                        modal: true,
                        height: 250,
                        width: 400,
                        resizable: false,
                        closeOnEscape: true,
                        title: "Notification",
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                                return;
                            }
                        }
                    });
                }
            };
            
            xhr.onerror = function(){
                $("#dialog").html("<p>An error occured while adding/update your booking</p>");
                $("#dialog").dialog({
                    modal: true,
                    height: 250,
                    width: 400,
                    resizable: false,
                    closeOnEscape: true,
                    title: "Error",
                    buttons: {
                        Ok: function() {
                            $(this).dialog("close");
                            return;
                        }
                    }
                });
                return;
            }
            
            var parm = "ts=" + myTrim(escape($("#date").val())) +
                        "&vehicle=" + myTrim(escape($( "#vehicle option:selected" ).text())) +
                        "&duration=" + myTrim(escape($("#duration").val())) +
                        "&distance=" + myTrim(escape($("#distance").val())) +
                        "&reason=" + myTrim(escape($("#reason").val())) +
                        "&east=" + $("#east").val();
                        
            xhr.open("POST", "/car_booking/addBooking.php", true);
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send(parm);
        }else{
            $("#dialog").html("<p>Please fill in all the fields in this form to book a vehicle</p>");
            $("#dialog").dialog({
                modal: true,
                height: 250,
                width: 400,
                resizable: false,
                closeOnEscape: true,
                title: "An error occured",
                buttons: {
                    Ok: function() {
                        $(this).dialog("close");
                        return;
                    }
                }
            });
        }
        
    });
}

/*
 * trim text
 */
function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}

/*
 * add listener for when the logout button is clicked
 */
function initLogoutListener() {
    $("#logout").click(function() {
        xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {         
                var resp = JSON.parse(this.response);
                if (resp["error"] == false){
                    window.location.replace(resp["error_msg"]);
                }else{
                    $("#dialog").html(resp["error_msg"]);
                    $("#dialog").dialog({
                        modal: true,
                        height: 250,
                        width: 400,
                        resizable: false,
                        closeOnEscape: true,
                        title: "Login Failed",
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                                return;
                            }
                        }
                    });
                }
            }
        };
        
        xhr.onerror = function(){
            $("#dialog").html("An error occured while logging out");
            $("#dialog").dialog({
                modal: true,
                height: 250,
                width: 400,
                resizable: false,
                closeOnEscape: true,
                title: "Login Failed",
                buttons: {
                    Ok: function() {
                        $(this).dialog("close");
                        return;
                    }
                }
            });
        }
    
        xhr.open("POST", "/car_booking/logout.php", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send();
    });
}