<?php
require_once('shared/session_lib.php');
require "readIni.php";

ini_set('display_errors', '0');
//no parameters required, just call the function
$ini = getIni();

//access database credentials using ini to avoid using literals in code for sensitive info
$MYSQLCONN = mysqli_connect($ini["db"]["server_name"],
                            $ini["db"]["username"],
                            $ini["db"]["password"],
                            $ini["db"]["database"]);

if (!$MYSQLCONN){
  echo returnJsonError("Failed to connect to server, please try again", true);
  return;
}
    
// Check connection
if (mysqli_connect_errno()){
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    return;
}

// Get SESSION_ID cookie
$session_id_cookie = isset($_COOKIE["SESSION_ID"]) ? $_COOKIE["SESSION_ID"] : null;

// Now expire the cookie
setcookie( "SESSION_ID", $session_id_cookie,time() - 3600,'/', null,1,1);

// escape these inputs ready for MySql
$session_id_cookie = mysqli_real_escape_string($MYSQLCONN, $session_id_cookie);

// Find session_id (based on the cookie we got earlier);
    $lc_sql =<<<END
	SELECT car_booking.user_validate_session('$session_id_cookie') AS valid_session_id;
END;
    $lc_result = mysqli_query($MYSQLCONN, $lc_sql) or die(returnJsonError("Errer getting session", true));
    while($lc_row = mysqli_fetch_array($lc_result)){
	$valid_session_id = $lc_row['valid_session_id'];
    }
    // free up query results
    mysqli_free_result($lc_result);

    // In user_session table, set expires and logout_datetime to now();
    // Whats the purpose of the logout_datetime field? Do we really need this???
    if($valid_session_id != null) {
	$lc_sql =<<<END
	    UPDATE car_booking.user_session SET expires=now() WHERE session_id=$valid_session_id;
END;
	mysqli_query($MYSQLCONN, $lc_sql) or die(returnJsonError("Error closing session", true)) ;
    }

//redirect to login page.
$response["error"] = false;
$response["error_msg"] = "login.php";
echo json_encode($response);
$MYSQLCONN->close();
return;
?>