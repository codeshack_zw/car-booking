<?php


////////////////////////////////////////////////////
// Redirect to a URL
// -------------------------------------------------
// INPUT
// $url  -required- URL to redirect to
////////////////////////////////////////////////////
function redirect($url){
    if (!headers_sent()){ //If headers not sent yet... then do php redirect
        header('Location: '.$url);
        exit;
    }
    else { //If headers are sent... do javascript redirect... if javascript disabled, do html redirect.
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
        exit;
    }
}

////////////////////////////////////////////////////
// Generate a Random Alpha Numeric String
// -------------------------------------------------
// INPUT
// $length
// -------------------------------------------------
// OUTPUT
// $randomstring
////////////////////////////////////////////////////
function randomAlphaNum($lenth=5) {
    // makes a random alpha numeric string of a given lenth
    $aZ09 = array_merge(range('A', 'Z'), range('a', 'z'),range(0, 9));
    $out ='';
    for($c=0;$c < $lenth;$c++) {
       $out .= $aZ09[mt_rand(0,count($aZ09)-1)];
    }
    return $out;
} 

function returnJsonError($msg, $tag){
    $response["error"] = $tag;
    $response["error_msg"] = $msg;
    return json_encode($response);
}

function validateSessionPage($MYSQLCONN, $login_url = "/car_booking/login.php") {

    // Get SESSION_ID cooike and REQUEST_URI
    $session_id = isset($_COOKIE["SESSION_ID"]) ? $_COOKIE["SESSION_ID"] : null;
    // and escape these inputs ready for MySql
    $session_id = mysqli_real_escape_string($MYSQLCONN, $session_id);
    
    $lc_sql =<<<END
	SELECT 
	s.session_id,s.expires,u.*,
	car_booking.user_validate_session('$session_id') AS valid_session_id
	FROM car_booking.user_session AS s
	INNER JOIN car_booking.user_info AS u ON u.user_id=s.user_id
	HAVING s.session_id=valid_session_id AND now() <= s.expires
END;

    $lc_result = mysqli_query($MYSQLCONN, $lc_sql) or die("Failed to validate session");
    while($lc_row = mysqli_fetch_array($lc_result)){
        $user_id = $lc_row['user_id'];
        $session_id = $lc_row['valid_session_id'];
    }
    
    // free up results
    mysqli_free_result($lc_result); // free up results

    ///////////////////
    // Validate page //
    ///////////////////		
    if($user_id != null) {	
	// SUCCESS SO session authenticated
	return array($user_id, $session_id);
    }
    // validation has failed, they shouldn't be here
    // Do something or redirect to login page
    else {
    	//invalid session go back to login page
	redirect($login_url);
    }

} 

?>
